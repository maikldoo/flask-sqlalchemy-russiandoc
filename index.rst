.. flask-sqlalchemy-russian documentation master file, created by
   sphinx-quickstart on Wed Dec 21 15:04:23 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

:orphan:

Flask-SQLAlchemy
================

.. module:: flask_sqlalchemy

Перевод документаци `Flask-sqlalchemy`_
Flask-SQLAlchemy это расширение для `Flask`_ добовляющее поддрежку `SQLAlchemy`_
для ваших приложений. Для работы требуется SQLAlchemy 0.6 или выше. Он призван
упростить работу во Flask с SQLAlchemy предоставляя удобные стандартные и дополнительные
подходы, для упрощения выполнения основных задач.

.. _Flask-sqlalchemy: http://flask-sqlalchemy.pocoo.org/2.1/
.. _SQLAlchemy: http://www.sqlalchemy.org/
.. _Flask: http://flask.pocoo.org/

.. include:: contents.rst.inc
